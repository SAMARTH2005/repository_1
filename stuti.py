import turtle

if __name__ == '__main__':
    t = turtle.Turtle()
    t.color('red')
    t.screen.bgcolor('black')
    t.pensize(50)
    t.penup()
    t.setpos(-200, -70)
    t.lt(90)
    t.pendown()
    t.fd(250)
    t.rt(120)
    t.fd(500)
    t.rt(150)
    t.fd(430)
    turtle.done()

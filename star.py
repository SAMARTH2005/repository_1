from turtle import done

import turtle

if __name__ == '__main__':
    t = turtle.Turtle()
    t.speed('fast')
    t.setposition(-400, 0)
    t.shape('circle')
    t.screen.bgcolor('black')
    t.pensize(5)
    t.color('red', 'yellow')
    t.begin_fill()
    while True:
        t.forward(800)
        t.left(170)
        t.stamp()
        if round(t.xcor()) == -400 and round(t.ycor()) == 0:
            break
    t.end_fill()
    done()

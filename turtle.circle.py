import turtle

if __name__ == '__main__':
    tim = turtle.Turtle()
    tim.pencolor('red')
    tim.pensize(3)
    tim.penup()
    tim.backward(200)
    tim.pendown()
    tim.circle(200)

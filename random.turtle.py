import random
import turtle

if __name__ == '__main__':
    r = random.Random()
    t = turtle.Turtle()
    i = r.randrange(1, 6)
    t.pensize(8)
    t.shape('arrow')
    if i == 1:
        t.color('red')
        t.bgcolor('black')
    elif i == 2:
        t.color('blue')
        t.bgcolor('yellow')
    elif i == 3:
        t.color('orange')
        t.bgcolor('blue')
    elif i == 4:
        t.color('green')
        t.bgcolor('blue')
    else:
        t.color('black')
        t.bgcolor('white')
    n = 0
    if i == 1:
        t.fillcolor('black')
        t.circle(100)

    elif i == 2:
        t.fillcolor('yellow')
        t.fd(100)
        t.rt(90)
        t.fd(100)
        t.rt(90)
        t.fd(100)
        t.rt(90)
        t.fd(100)
        t.rt(90)

    elif i == 3:
        t.fillcolor('blue')
        t.fd(300)
        t.lt(120)
        t.fd(300)
        t.lt(120)
        t.fd(300)
        t.lt(120)

    elif i == 4:
        t.fillcolor('red')
        t.fd(150)
        t.lt(72)
        t.fd(150)
        t.lt(72)
        t.fd(150)
        t.lt(72)
        t.fd(150)
        t.lt(72)
        t.fd(150)
        t.lt(72)

    else:
        t.fillcolor('green')
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        t.fd(250)
        t.lt(160)
        n += 1
    print(i)

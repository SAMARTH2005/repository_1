import turtle

if __name__ == '__main__':
    t = turtle.Turtle()
    t.shape('turtle')
    t.screen.bgcolor("black")
    t.color("white")
    t.pensize(5)


    def up():
        t.rt(135)
        t.fd(500)


    t.screen.onkey(up, "Up")
    t.screen.listen()


    def dn():
        t.lt(90)
        t.fd(500)


    t.screen.onkey(dn, "Down")
    t.screen.listen()
    turtle.done()

import turtle
if __name__ == '__main__':
    t = turtle.Turtle()
    t.screen.bgcolor('black')
    t.penup()
    t.setpos(-500, 199)
    t.shape("turtle")
    t.speed('fastest')
    t.pendown()
    t.pensize(3)
    t.pencolor('white')
    t.rt(90)
    while t.ycor() < 199.1:
        t.fd(450)
        t.bk(400)
        t.lt(5)

    turtle.done()




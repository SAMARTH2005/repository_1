import turtle

if __name__ == '__main__':
    t = turtle.Turtle()


    def spu():
        t.speed('fast')


    def spd():
        t.speed("slowest")


    t.screen.onkey(spu, 'Up')
    t.screen.listen()
    t.screen.onkey(spd, 'Down')
    t.screen.listen()
    t.pencolor('orange')
    t.pensize(234)
    t.shape('turtle')
    t.penup()
    t.bk(400)
    t.lt(90)
    t.fd(280)
    t.rt(90)
    t.pendown()
    t.fd(800)
    t.rt(90)
    t.penup()
    t.color('green')
    t.fd(560)
    t.pendown()
    t.rt(90)
    t.fd(800)
    t.rt(90)
    t.penup()
    t.fd(117)
    t.rt(90)
    t.fd(400)
    t.color('blue')
    t.pensize(3)
    t.pendown()
    t.circle(162)
    t.lt(90)
    t.fd(324)
    t.bk(162)
    t.lt(90)
    t.fd(162)
    t.bk(162)

    n = 0
# noinspection PyUnboundLocalVariable
while n < 24:
    # noinspection PyUnboundLocalVariable
    t.lt(15)
    t.fd(162)
    t.bk(162)
    n += 1

turtle.done()

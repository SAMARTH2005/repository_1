import tkinter as tk


def drawstar():
    import turtle

    if __name__ == '__main__':
        t = turtle.Turtle()
        t.speed('fast')
        t.setposition(-400, 0)
        t.shape('circle')
        t.screen.bgcolor('black')
        t.pensize(5)
        t.color('red', 'yellow')
        while True:
            t.forward(800)
            t.left(170)
            t.stamp()
            if round(t.xcor()) == -400 and round(t.ycor()) == 0:
                break
        t.screen.clear()
        t.screen.listen()


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.flag = tk.Button(self)
        self.quit = tk.Button(self, text="QUIT", fg="white", bg="red", command=self.master.destroy)
        self.star = tk.Button(self)
        self.my_first_app = tk.Button(self)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.my_first_app["text"] = "Hello! I'm SAMARTH'S First App\nCOOL, Right?"
        self.my_first_app["fg"] = "skyblue"
        self.my_first_app["bg"] = "black"
        self.my_first_app["command"] = self.say_hi
        self.my_first_app.pack(side="top")

        self.flag["text"] = "Click to see an animated Indian Flag."
        self.flag["fg"] = "orange"
        self.flag["bg"] = "green"
        self.flag["command"] = self.drawflag
        self.flag.pack(side="left")

        self.star["text"] = "Click to see an animated Star."
        self.star["fg"] = "red"
        self.star["bg"] = "yellow"
        self.star["command"] = drawstar
        self.star.pack(side="right")

        self.quit.pack(side="bottom")

    @staticmethod
    def say_hi():
        """

        """
        print("Hello Everyone!")

    @staticmethod
    def drawflag():
        import turtle

        if __name__ == '__main__':
            t = turtle.Turtle()

            def spu():
                t.speed('fast')

            def spd():
                t.speed("slowest")

            t.screen.onkey(spu, 'Up')
            t.screen.listen()
            t.screen.onkey(spd, 'Down')
            t.screen.listen()
            t.pencolor('orange')
            t.pensize(234)
            t.shape('turtle')
            t.penup()
            t.bk(400)
            t.lt(90)
            t.fd(280)
            t.rt(90)
            t.pendown()
            t.fd(800)
            t.rt(90)
            t.penup()
            t.color('green')
            t.fd(560)
            t.pendown()
            t.rt(90)
            t.fd(800)
            t.rt(90)
            t.penup()
            t.fd(117)
            t.rt(90)
            t.fd(400)
            t.color('blue')
            t.pensize(3)
            t.pendown()
            t.circle(162)
            t.lt(90)
            t.fd(324)
            t.bk(162)
            t.lt(90)
            t.fd(162)
            t.bk(162)

            n = 0
        # noinspection PyUnboundLocalVariable
        while n < 24:
            # noinspection PyUnboundLocalVariable
            t.lt(15)
            t.fd(162)
            t.bk(162)
            n += 1
        t.screen.clear()
        t.screen.clearscreen()
        t.screen.listen()


Root = tk.Tk()
myapp = Application(master=Root)
myapp.master.title("SAMARTH")
myapp.master.maxsize(1000, 400)
myapp.mainloop()

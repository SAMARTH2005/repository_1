from turtle import Turtle

if __name__ == '__main__':

    tim: Turtle = Turtle()
    tim.pensize(5)
    tim.color('red')
    tim.shape('turtle')
    tim.fd(100)
    tim.lt(90)
    tim.fd(90)
    tim.rt(135)
    tim.fd(150)
    tim.rt(90)
    tim.fd(150)
    tim.rt(135)
    tim.fd(90)
    tim.lt(90)
    tim.fd(100)
